// 13/02/2018 by Dmitry Savchenko

import PureLayout
import SDWebImage

class ProductCell: UITableViewCell {

	required init?(coder aDecoder: NSCoder) { super.init(coder: aDecoder) }

	let container = 		UIStackView()

	let fieldsStack = 		UIStackView()
	let descriptionLabel =	UILabel()
	let retailerLabel = 	UILabel()

	let accessoryStack =	UIStackView()
	let productImageView =	UIImageView()
	@objc let button =		UIButton()

	let priceStack = 		UIStackView()
	let priceLabel =		UILabel()
	let discountLabel = 	UILabel()

	@objc var buttonAction: (ProductCell) -> () = { _ in }

	private static let spacing = CGFloat(8)
	private static let accessoryItemSize = CGSize(width: 72, height: 72)

	private static let cellForComputingHeight =
		ProductCell(style: .default, reuseIdentifier: String(describing: ProductCell.self))

	override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)

		contentView.addSubview(fieldsStack)
		contentView.addSubview(accessoryStack)

		fieldsStack.autoPinEdge(.top, to: .top, of: contentView)
		fieldsStack.autoPinEdge(.leading, to: .leading, of: contentView)
		accessoryStack.autoPinEdge(.top, to: .top, of: contentView)
		accessoryStack.autoPinEdge(.trailing, to: .trailing, of: contentView)

		fieldsStack.autoPinEdge(.trailing, to: .leading, of: accessoryStack)

		let spacing = ProductCell.spacing
		let accessoryItemSize = ProductCell.accessoryItemSize
		let margins = UIEdgeInsets(top: spacing, left: spacing, bottom: spacing, right: spacing)

		fieldsStack.spacing = spacing
		fieldsStack.axis = .vertical
		fieldsStack.isLayoutMarginsRelativeArrangement = true
		fieldsStack.layoutMargins = margins
		fieldsStack.addArrangedSubview(descriptionLabel)
		fieldsStack.addArrangedSubview(retailerLabel)
		fieldsStack.addArrangedSubview(priceStack)
		descriptionLabel.numberOfLines = 0

		accessoryStack.spacing = spacing
		accessoryStack.axis = .vertical
		accessoryStack.isLayoutMarginsRelativeArrangement = true
		accessoryStack.layoutMargins = margins
		accessoryStack.addArrangedSubview(productImageView)
		accessoryStack.addArrangedSubview(button)
		productImageView.contentMode = .scaleAspectFit
		productImageView.autoSetDimensions(to: accessoryItemSize)
		button.layer.masksToBounds = true
		button.layer.cornerRadius = min(accessoryItemSize.width, accessoryItemSize.height) / 2
		button.addTarget(self, action: #selector(buttonDidPress), for: .touchUpInside)
		button.autoSetDimensions(to: accessoryItemSize)
		button.titleLabel?.textColor = .white

		priceStack.spacing = spacing
		priceStack.distribution = .fillEqually
		priceStack.addArrangedSubview(priceLabel)
		priceStack.addArrangedSubview(discountLabel)

		let labels = [descriptionLabel, retailerLabel, priceLabel, discountLabel]
		labels.forEach {
			$0.backgroundColor = .groupTableViewBackground
		}
	}

	@objc private func buttonDidPress() {
		buttonAction(self)
	}
}

extension ProductCell {

	@objc func fill(with item: ProductCellItem) {

		fillFields(with: item)

		retailerLabel.isHidden = item.retailer == nil
		priceLabel.isHidden = item.price == nil
		discountLabel.isHidden = item.discount == nil
		productImageView.isHidden = item.retailer == nil

		button.setTitle(item.buttonText, for: .normal)
		button.backgroundColor = item.buttonBackgroundColor

		if let urlString = item.image {
			productImageView.sd_setImage(with: URL(string: urlString))
		} else {
			productImageView.sd_cancelCurrentImageLoad()
			productImageView.image = nil
		}
	}

	static func height(with item: ProductCellItem, cellWidth: CGFloat) -> CGFloat {

		let cell = cellForComputingHeight
		cell.fillFields(with: item)

		let accessoryStackHeight = accessoryItemSize.height * 2 + spacing * 3
		let accessoryStackWidth = accessoryItemSize.width + spacing * 2

		var fieldsStackHeight = CGFloat(0)

		let fieldsMaxWidth = (cellWidth - accessoryStackWidth) - spacing * 2
		let heightThatFits = { label in
			return label.sizeThatFits(CGSize(width: fieldsMaxWidth, height: .greatestFiniteMagnitude)).height
			} as (UILabel) -> (CGFloat)

		let descriptionHeight = heightThatFits(cell.descriptionLabel)
		fieldsStackHeight += descriptionHeight + spacing

		if item.retailer != nil {
			let retailerHeight = heightThatFits(cell.retailerLabel)
			fieldsStackHeight += retailerHeight + spacing
		}

		if item.price != nil {
			let priceHeight = heightThatFits(cell.priceLabel)
			fieldsStackHeight += priceHeight + spacing
		} else if item.discount != nil {
			let discountHeight = heightThatFits(cell.discountLabel)
			fieldsStackHeight += discountHeight + spacing
		}

		fieldsStackHeight += spacing

		return max(accessoryStackHeight, fieldsStackHeight)
	}

	private func fillFields(with item: ProductCellItem) {
		descriptionLabel.text = item.about
		retailerLabel.text = item.retailer
		priceLabel.text = item.price
		discountLabel.text = item.discount
	}
}
