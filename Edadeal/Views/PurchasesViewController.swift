// 09/02/2018 by Dmitry Savchenko

import UIKit
import PureLayout

class PurchasesViewController: UIViewController {

	let tableView = UITableView()

	weak var output: PurchasesViewOutput?

	required init?(coder aDecoder: NSCoder) { fatalError() }

	init(output: PurchasesViewOutput) {
		self.output = output

		super.init(nibName: nil, bundle: nil)

		self.title = "Список Покупок"
	}

	override func viewDidLoad() {
		super.viewDidLoad()

		view.addSubview(tableView)
		tableView.autoPinEdgesToSuperviewEdges()

		output?.viewDidLoad()
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)

		output?.viewWillAppear(animated)
	}
}
