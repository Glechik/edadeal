// 09/02/2018 by Dmitry Savchenko

#define let __auto_type const
#define var __auto_type

#import "SearchViewController.h"
#import "Edadeal-Swift.h"

@import PureLayout;

@interface SearchViewController () <UISearchBarDelegate>

@property (nonatomic, weak) id<SearchViewOutput> output;

@end

@implementation SearchViewController

- (instancetype)initWithOutput:(id<SearchViewOutput>)output {
	self = [super init];

	self.title = @"Поиск";
	self.output = output;

	return self;
}

- (void)viewDidLoad {
	[super viewDidLoad];

	self.edgesForExtendedLayout = UIRectEdgeNone;

	self.view.backgroundColor = UIColor.grayColor;

	let searchBar = UISearchBar.new;
	[self.view addSubview:searchBar];
	[searchBar autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsZero
										excludingEdge:ALEdgeBottom];
	self.searchBar = searchBar;

	let tableView = UITableView.new;
	[self.view addSubview:tableView];
	[tableView autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:searchBar];
	[tableView autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsZero
										excludingEdge:ALEdgeTop];
	tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;
	self.tableView = tableView;

	let loadingView = LoadingView.new;
	[self.view addSubview:loadingView];
	[loadingView autoCenterInSuperview];
	self.loadingView = loadingView;

	[self.output viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];

	[self.output viewWillAppear:animated];
}

@end

