// 13/02/2018 by Dmitry Savchenko

import PureLayout

class LoadingView: UIView {

	required init?(coder aDecoder: NSCoder) { super.init(coder: aDecoder) }

	private let stackView = UIStackView()

	private let retryButton = UIButton()
	private let activityView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
	private let textLabel = UILabel()

	@objc var retryBlock = {}

	init() {
		super.init(frame: .zero)

		backgroundColor = .white

		layer.masksToBounds = true
		layer.cornerRadius = 8

		activityView.startAnimating()

		retryButton.setImage(#imageLiteral(resourceName: "retry"), for: .normal)
		retryButton.addTarget(self, action: #selector(retryClick), for: .touchUpInside)
		retryButton.autoSetDimensions(to: CGSize(width: 44, height: 44))

		textLabel.textColor = .darkText
		textLabel.font = UIFont.systemFont(ofSize: textLabel.font.pointSize, weight: .medium)

		stackView.spacing = 26
		stackView.layoutMargins = UIEdgeInsets(top: 32, left: 36, bottom: 32, right: 36)
		stackView.isLayoutMarginsRelativeArrangement = true
		addSubview(stackView)
		stackView.autoPinEdgesToSuperviewEdges()

		stackView.addArrangedSubview(activityView)
		stackView.addArrangedSubview(textLabel)
		stackView.addArrangedSubview(retryButton)

		showLoading()
	}

	@objc func showLoading() {
		textLabel.text = "Загрузка данных"

		activityView.isHidden = false
		retryButton.isHidden = true
	}

	@objc func showFail() {
		textLabel.text = "Ошибка загрузки"

		activityView.isHidden = true
		retryButton.isHidden = false
	}

	@objc private func retryClick() {
		retryBlock()
	}
}
