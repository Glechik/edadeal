// 09/02/2018 by Dmitry Savchenko

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class LoadingView;

@protocol SearchViewOutput;

@interface SearchViewController : UIViewController

@property (nullable, nonatomic) LoadingView *loadingView;
@property (nullable, nonatomic, weak) UITableView *tableView;
@property (nullable, nonatomic, weak) UISearchBar *searchBar;

- (instancetype)initWithOutput:(nullable id<SearchViewOutput>)output;

@end

NS_ASSUME_NONNULL_END
