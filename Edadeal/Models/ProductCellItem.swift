// 15/02/2018 by Dmitry Savchenko

class ProductCellItem: NSObject {

	@objc enum ActionType: Int {
		case add
		case delete
	}

	let about: String
	let price: String?
	let discount: String?
	let image: String?
	let retailer: String?

	let buttonBackgroundColor: UIColor
	let buttonText: String

	@objc init(product: Product, actionType: ProductCellItem.ActionType) {

		about = product.about!
		retailer = product.retailer
		image = product.image

		if let price = product.price {
			self.price = "\(price.stringValue) ₽"
		} else {
			self.price = nil
		}

		if let discount = product.discount {
			self.discount = "\(discount.stringValue) %"
		} else {
			self.discount = nil
		}

		switch actionType {
		case .add:
			buttonText = "Add"
			buttonBackgroundColor = product.inCart ? .green : .gray
		case .delete:
			buttonText = "Remove"
			buttonBackgroundColor = .gray
		}
	}
}
