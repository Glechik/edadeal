// 12/02/2018 by Dmitry Savchenko

extension Product {

	struct Parse: Decodable {
		let description: String
		let price: Float?
		let discount: Int?
		let image: String?
		let retailer: String?
	}

	func update(with parse: Parse) {
		about = parse.description
		if let price = parse.price 			{ self.price = NSNumber(value: price) }
		if let discount = parse.discount 	{ self.discount = NSNumber(value: discount) }
		image = parse.image
		retailer = parse.retailer
	}
}

