// 12/02/2018 by Dmitry Savchenko

import CoreData

class StorageService {

	private let dataStack = CoreDataStack()

	var productsIsEmpty: Bool {

		let context = dataStack.backgroundContext
		let request = NSFetchRequest<Product>(entityName: String(describing: Product.self))
		do {
			let count  = try context.count(for: request)
			return count == 0 ? true : false
		} catch {
			return true
		}
	}

	@objc func fetchProducts(withAboutSubstring substring: String) throws -> [Product] {

		let context = dataStack.viewContext
		let request = NSFetchRequest<Product>(entityName: String(describing: Product.self))
		request.predicate = NSPredicate(format: "%K contains[c] %@", #keyPath(Product.about), substring)

		return try context.fetch(request)
	}

	@objc func fetchProductsInCart() throws -> [Product] {
		let context = dataStack.viewContext
		let request = NSFetchRequest<Product>(entityName: String(describing: Product.self))
		request.predicate = NSPredicate(format: "%K == TRUE", #keyPath(Product.inCart))

		return try context.fetch(request)
	}

	func create(products: [Product.Parse]) throws {

		let context = dataStack.backgroundContext
		products.forEach {
			let product = NSEntityDescription.insertNewObject(forEntityName: String(describing: Product.self), into: context) as! Product
			product.update(with: $0)
		}

		try context.save()
	}
}
