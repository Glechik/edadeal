// 14/02/2018 by Dmitry Savchenko

import CoreData

class CoreDataStack {

	private lazy var managedObjectModel: NSManagedObjectModel = {
		let modelURL = Bundle.main.url(forResource: "Edadeal", withExtension: "momd")!
		return NSManagedObjectModel(contentsOf: modelURL)!
	}()

	private lazy var coordinator: NSPersistentStoreCoordinator = {
		return NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
	}()

	private var persistentStoreCoordinator: NSPersistentStoreCoordinator {
		if coordinator.persistentStores.count > 0 {
			return coordinator
		}

		do {
			var storeURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last
			storeURL?.appendPathComponent("Edadeal.sqlite")
			try coordinator.addPersistentStore(ofType: NSSQLiteStoreType,
											   configurationName: nil,
											   at: storeURL,
											   options: [NSMigratePersistentStoresAutomaticallyOption: true,
														 NSInferMappingModelAutomaticallyOption: true]
			)
		} catch {
			print("CoreData error \(error)")
		}

		return coordinator
	}

	lazy var viewContext: NSManagedObjectContext = {
		var context = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
		context.persistentStoreCoordinator = self.persistentStoreCoordinator
		return context
	}()

	lazy var backgroundContext: NSManagedObjectContext = {
		var context = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
		context.persistentStoreCoordinator = self.persistentStoreCoordinator
		return context
	}()
}
