// 15/02/2018 by Dmitry Savchenko

class RemoteService {

	private let session = URLSession.shared
	private var task: URLSessionDataTask?

	func requestProducts(success: @escaping (Data) -> (),
						 failure: @escaping (NSString) -> ()) {

		guard let url = URL(string: "https://api.edadev.ru/intern/") else {
			failure("Wrong URL") ; return
		}

		var request = URLRequest(url: url)
		request.timeoutInterval = 10

		task = session.dataTask(with: request) { [weak self] data, response, error in

			guard let `self` = self else {
				failure("RemoteService was released") ; return
			}

			defer { self.task = nil }

			guard error == nil else {
				failure(error!.localizedDescription as NSString) ; return
			}

			guard let data = data else {
				failure("Empty data") ; return
			}

			success(data)
		}

		task?.resume()
	}
}
