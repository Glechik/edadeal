// 10/02/2018 by Dmitry Savchenko

class ProductsService: NSObject {

	private let remoteService = RemoteService()
	private let storageService = StorageService()

	private let searchQueue = DispatchQueue(label: "my.Edadeal.searchProducts", qos: .userInitiated)
}

extension ProductsService {

	@objc func loadProducts(success: @escaping () -> (),
							failure: @escaping (NSString) -> ()) {

		guard self.storageService.productsIsEmpty else {
			success() ; return
		}

		let success = { DispatchQueue.main.async { success() } }
		let failure = { cause in DispatchQueue.main.async { failure(cause as NSString) } }

		remoteService.requestProducts(success: { data in

			let decoder = JSONDecoder()
			guard let products = try? decoder.decode([Product.Parse].self, from: data) else {
				failure("Products parse error") ; return
			}

			do {
				try self.storageService.create(products: products)
				success()
			} catch {
				failure("Save to storage error")
			}

		}) { cause in
			failure(cause)
		}
	}
}

extension ProductsService {

	@objc func searchProducts(withAboutSubstring substring: String,
							  completionBlock: @escaping ([Product]) -> ()) -> Operation {

		let searchOperation = BlockOperation()

		searchQueue.async {
			searchOperation.addExecutionBlock {

				let completionBlock = { products in DispatchQueue.main.async { completionBlock(products) } }

				do {
					let products = try self.storageService.fetchProducts(withAboutSubstring: substring)

					guard searchOperation.isCancelled == false else { return; }

					let sortedProducts = products.sorted { first, second -> Bool in
						let firstIndex = first.about!.range(of: substring, options: .caseInsensitive)!.lowerBound
						let secondIndex = second.about!.range(of: substring, options: .caseInsensitive)!.lowerBound
						return firstIndex < secondIndex
					}

					guard searchOperation.isCancelled == false else { return; }

					completionBlock(sortedProducts)
				} catch {
					completionBlock([])
				}
			}
			searchOperation.start()
		}

		return searchOperation
	}
}

extension ProductsService {

	func productsInCart() -> [Product] {
		do {
			return try storageService.fetchProductsInCart()
		} catch {
			return []
		}
	}
}

extension ProductsService {

	@objc func addProdcutToCart(_ prodcut: Product) {

		prodcut.inCart = true

		do {
			try prodcut.managedObjectContext?.save()
		} catch let error as NSError {
			print(error.localizedDescription)
		}
	}

	@objc func removeProdcutFromCart(_ prodcut: Product) {

		prodcut.inCart = false

		do {
			try prodcut.managedObjectContext?.save()
		} catch let error as NSError {
			print(error.localizedDescription)
		}
	}
}
