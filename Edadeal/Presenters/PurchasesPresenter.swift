// 15/02/2018 by Dmitry Savchenko

class PurchasesPresenter: NSObject {

	private let productsService: ProductsService

	private var products = [Product]()

	lazy var view = { return PurchasesViewController(output: self) }()

	init(productsService: ProductsService) {
		self.productsService = productsService
	}
}

extension PurchasesPresenter: PurchasesViewOutput {

	func viewDidLoad() {
		view.tableView.register(ProductCell.self, forCellReuseIdentifier: String(describing: ProductCell.self))
		view.tableView.dataSource = self
		view.tableView.delegate = self
	}

	func viewWillAppear(_ animated: Bool) {
		products = self.productsService.productsInCart()
		view.tableView.reloadData()
	}
}

extension PurchasesPresenter: UITableViewDataSource {

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.products.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ProductCell.self), for: indexPath) as! ProductCell

		let product = self.products[indexPath.row]
		let item = ProductCellItem(product: product, actionType: .delete)
		cell.fill(with: item)

		cell.buttonAction = { [weak self] cell in
			guard let `self` = self else {
				return
			}

			self.productsService.removeProdcutFromCart(product)

			guard let indexPath = self.view.tableView.indexPath(for: cell) else {
				return
			}
			self.view.tableView.beginUpdates()
			self.products.remove(at: indexPath.row)
			self.view.tableView.deleteRows(at: [indexPath], with: .automatic)
			self.view.tableView.endUpdates()
		}

		return cell
	}
}

extension PurchasesPresenter: UITableViewDelegate {

	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

		let product = products[indexPath.row]
		let item = ProductCellItem(product: product, actionType: .delete)
		return ProductCell.height(with: item, cellWidth: tableView.frame.width)
	}
}
