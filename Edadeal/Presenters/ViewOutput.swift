// 15/02/2018 by Dmitry Savchenko

import UIKit

@objc protocol ViewOutput {
	func viewDidLoad()
	func viewWillAppear(_ animated: Bool)
}

extension ViewOutput {
	func viewDidLoad() {}
	func viewWillAppear(_ animated: Bool) {}
}

// MARK: -

@objc protocol SearchViewOutput: ViewOutput, UISearchBarDelegate {}
protocol PurchasesViewOutput: ViewOutput {}
