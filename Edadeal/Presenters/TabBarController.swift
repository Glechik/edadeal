// 09/02/2018 by Dmitry Savchenko

import UIKit

class TabBarController: UITabBarController {

	required init?(coder aDecoder: NSCoder) { fatalError() }

	private let searchPresenter: SearchPresenter
	private let purchasesPresenter: PurchasesPresenter

	init() {
		let productsService = ProductsService()

		searchPresenter = SearchPresenter(productsService: productsService)
		let searchNavigationController = UINavigationController(rootViewController: searchPresenter.view)
		searchNavigationController.tabBarItem.image = #imageLiteral(resourceName: "search")

		purchasesPresenter = PurchasesPresenter(productsService: productsService)
		let purchasesNavigationController = UINavigationController(rootViewController: purchasesPresenter.view)
		purchasesNavigationController.tabBarItem.title = "Покупки"
		purchasesNavigationController.tabBarItem.image = #imageLiteral(resourceName: "cart")

		super.init(nibName: nil, bundle: nil)

		viewControllers = [searchNavigationController, purchasesNavigationController]
	}
}
