// 15/02/2018 by Dmitry Savchenko

class SearchPresenter: NSObject {

	private var searchOperation: Operation?

	private let productsService: ProductsService

	private var products = [Product]()

	lazy var view = { return SearchViewController(output: self) }()

	init(productsService: ProductsService) {
		self.productsService = productsService
	}
}

extension SearchPresenter {

	func loadProducts() {

		view.loadingView?.showLoading()

		productsService.loadProducts(success: { [weak self] in

			self?.view.loadingView?.removeFromSuperview()

			self?.view.searchBar?.isHidden = false
			self?.view.tableView?.isHidden = false

		}) { [weak self] cause in
			self?.view.loadingView?.showFail()
		}
	}
}

extension SearchPresenter: SearchViewOutput {

	func viewDidLoad() {
		view.tableView?.register(ProductCell.self, forCellReuseIdentifier: String(describing: ProductCell.self))
		view.tableView?.dataSource = self
		view.tableView?.delegate = self

		view.searchBar?.isHidden = true
		view.tableView?.isHidden = true
		view.loadingView?.isHidden = false

		view.loadingView?.retryBlock = { [weak self] in
			self?.loadProducts()
		}

		view.searchBar?.placeholder = "Введите название продукта"
		view.searchBar?.delegate = self

		loadProducts()
	}

	func viewWillAppear(_ animated: Bool) {
		view.tableView?.reloadData()
	}
}

extension SearchPresenter: UITableViewDataSource {

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.products.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ProductCell.self), for: indexPath) as! ProductCell

		let product = products[indexPath.row]
		let item = ProductCellItem(product: product, actionType: .add)
		cell.fill(with: item)

		cell.buttonAction = { [weak self] cell in

			if product.inCart {
				self?.productsService.removeProdcutFromCart(product)
			} else {
				self?.productsService.addProdcutToCart(product)
			}
			cell.button.backgroundColor = product.inCart ? .green : .gray
		}

		return cell
	}


}

extension SearchPresenter: UITableViewDelegate {

	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

		let product = products[indexPath.row]
		let item = ProductCellItem(product: product, actionType: .add)
		return ProductCell.height(with: item, cellWidth: tableView.frame.width)
	}
}

extension SearchPresenter: UISearchBarDelegate {

	func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
		guard searchText.count > 1 else {
			products = []
			view.tableView?.reloadData()
			return
		}

		let completionBlock = { [weak self] products in
			self?.products = products
			self?.view.tableView?.reloadData()
		} as ([Product]) -> ()

		searchOperation?.cancel()
		searchOperation = productsService.searchProducts(withAboutSubstring: searchText,
														 completionBlock: completionBlock)
	}
}
